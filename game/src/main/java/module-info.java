module cz.cvut.fel.pjv.sykoro10.game {
    requires javafx.controls;
    requires javafx.fxml;


    opens cz.cvut.fel.pjv.sykoro10.game to javafx.fxml;
    exports cz.cvut.fel.pjv.sykoro10.game;
}