package cz.cvut.fel.pjv.sykoro10.game;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.lang.Thread.sleep;

public class GameLoop {
    public Game game;
    private boolean paused;

    public GameLoop(Game game){
        this.game = game;
    }

    public void start() {
        ExecutorService executorService = Executors.newSingleThreadExecutor();

        executorService.submit(() -> {
            while (!paused) {
                System.out.println("Started");
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                game.update(0, 0);
                game.render();
            }
        });
    }

    public void pause(){
        if (paused){
            start();
        }
        this.paused = !paused;
    }

}
