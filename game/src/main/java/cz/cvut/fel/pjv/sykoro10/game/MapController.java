package cz.cvut.fel.pjv.sykoro10.game;


import javafx.animation.AnimationTimer;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyEvent;
import javafx.event.EventHandler;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class MapController implements Initializable {


    @FXML
    private Canvas canvas;
    GraphicsContext context;
    GameLoop gameLoop;
    Game game;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        this.context = canvas.getGraphicsContext2D();
        /*context.setFill(Color.SEAGREEN);
        context.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
        this.player = new Player(0, 0);
        player.setImage("/org/Colour1/Outline/120x80_gifs/__Idle.gif");
        player.draw(canvas.getGraphicsContext2D());*/
        this.game = new Game();
        game.initialize();
        this.gameLoop = new GameLoop(game);
        game.setGraphicsContext(context, canvas.getWidth(), canvas.getHeight());
        gameLoop.start();



    }

    public EventHandler<KeyEvent> getKeyHandler() {
        return keyEvent -> {
            switch (keyEvent.getCode()) {
                case W -> System.out.println("W");
                case A -> System.out.println("A");
                case S -> System.out.println("S");
                case D -> System.out.println("D");
                case SPACE -> System.out.println("space (dash)");
                case F -> System.out.println("F (interact)");
                case E -> System.out.println("E (inventory)");
                case ENTER -> System.out.println("enter (attack)");
                case P -> this.gameLoop.pause();
            }
        };
    }

}
