package cz.cvut.fel.pjv.sykoro10.game;

import javafx.scene.canvas.GraphicsContext;

public interface Character {

    void updatePos(double x, double y);

    void setImage(String filename);

    void draw(GraphicsContext graphicsContext);
}
