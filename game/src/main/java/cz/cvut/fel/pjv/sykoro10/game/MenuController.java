package cz.cvut.fel.pjv.sykoro10.game;

import javafx.animation.AnimationTimer;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.net.URL;
import java.util.ResourceBundle;

public class MenuController implements Initializable {
    @FXML
    private Button newGame, loadGame, settings, exit;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {    //idk jestli je tohle k necemu
        System.out.println("starting menu");

    }

    public void newGameClicked() throws Exception{  //dodelat nejak tu vyjimku
        Parent fxmlLoader = (Parent) FXMLLoader.load(Main.class.getResource("map.fxml"));
        Stage window = (Stage) newGame.getScene().getWindow();
        MapController mapController = new MapController();
        window.addEventHandler(KeyEvent.KEY_PRESSED, mapController.getKeyHandler());
        window.setScene(new Scene(fxmlLoader, 600, 400));
    }

    public void loadGameClicked(){
        System.out.println("load game");
    }

    public void settingsClicked() throws Exception{ //dodelat nejak tu vyjimku
        Parent fxmlLoader = (Parent) FXMLLoader.load(Main.class.getResource("settings.fxml"));
        Stage window = (Stage) settings.getScene().getWindow();
        window.setScene(new Scene(fxmlLoader, 600, 400));
    }

    public void exitClicked(){  //dodelat alert na neulozenou hru maybe?
        Stage window = (Stage) exit.getScene().getWindow();
        window.close();
    }

}
