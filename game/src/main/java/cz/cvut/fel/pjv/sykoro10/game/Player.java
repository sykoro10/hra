package cz.cvut.fel.pjv.sykoro10.game;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

public class Player implements Character {
    double posX;
    double posY;
    public Image image;
    public Object boundary;

    public Player(double x,double y){
        this.posX = x;
        this.posY = y;
        boundary = new Object(x, y, 0, 0);
    }

    public Object getBoundary(){
        boundary.x = posX;
        boundary.y = posY;
        return boundary;
    }

    public void setImage(String filename) {
        this.image = new Image(Player.class.getResourceAsStream(filename));
        boundary.width = image.getWidth();
        boundary.height = image.getHeight();
    }

    public void updatePos(double dx, double dy){
        posX += dx;
        posY += dy;
    }



    public void draw(GraphicsContext context){
        context.drawImage(image, posX, posY);
    }
}