package cz.cvut.fel.pjv.sykoro10.game;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class SettingsController {
    public Button back;

    public void backClicked() throws Exception{ //vyresit nejak vyjimku
        Parent fxmlLoader = (Parent) FXMLLoader.load(Main.class.getResource("menu.fxml"));
        Stage window = (Stage) back.getScene().getWindow();
        window.setScene(new Scene(fxmlLoader, 600, 400));
    }
}
