package cz.cvut.fel.pjv.sykoro10.game;

import javafx.scene.canvas.GraphicsContext;

import java.util.List;

public class Game {

    private GraphicsContext graphicsContext;
    double canvasWidth;
    double canvasHeight;
    private Player player;

    public void initialize(){
        this.player = new Player(0, 0);
        player.setImage("/org/Colour1/Outline/120x80_gifs/__Idle.gif");
    }

    public void setGraphicsContext(GraphicsContext graphicsContext, double canvasWidth, double canvasHeight){
        this.graphicsContext = graphicsContext;
        this.canvasWidth = canvasWidth;
        this.canvasHeight = canvasHeight;
    }
    public void update(double dx, double dy){
        player.updatePos(dx, dy);
    }
    public void render(){
        graphicsContext.clearRect(0, 0,canvasWidth, canvasHeight);
        player.draw(graphicsContext);
    }
}
